// 23:45 11/12/2561
// 2019/01/14 00:23:36
window.onload = function(e) {
	function getQueryStrings() {
		var assoc = {};
		var decode = function(s) {
			return decodeURIComponent(s.replace(/\+/g, " "));
		};
		var queryString = location.search.substring(1);
		var keyValues = queryString.split('&');
		for (var i in keyValues) {
			var key = keyValues[i].split('=');
			if (key.length > 1) {
				assoc[decode(key[0])] = decode(key[1]);
			}
		}
		return assoc;
	}
	// var _keywords = getQueryStrings()["h"]
	// document.getElementById("demo").innerHTML = _keywords ;
	// _keywords = _keywords.split(","); 
	var _keywords = getQueryStrings()["h"]
	if (_keywords == undefined) var _keywords = "๛";
	do {
		_keywords = _keywords.replace("," + ",", ",");
	}
	while (_keywords.indexOf(",,") > -1);
	var _keywordsLEN = _keywords.length
	if (_keywords[_keywordsLEN - 1] == ",") {
		_keywords = _keywords.slice(0, _keywordsLEN - 1)
	}
	if (_keywords[0] == ",") {
		_keywords = _keywords.slice(1)
	}
	var _ignored = "(<.*?>|\\n|\,)?"; //ไม่นับ tag,newline,ลูกน้ำ ทุกตำแหน่ง
	_keywords = _keywords.split(",").join('|') //ถ้าคำค้นใส่ "," แปลงเป็น "|" regex
		.split("").join(_ignored).split(" " + _ignored).join(' +') //เฉพาะถ้ามีช่องว่าง มีเท่าไหร่นับ 1 เสมอ
		.split(":" + _ignored).join('.*?') //เริ่มต้นด้วยคำก่อน : จบด้วยคำหลัง :
		.split("|" + _ignored).join('|');
	var _regExpPattern = new RegExp(`(${_keywords})`, 'igm');
	var _pageContent = document.getElementsByTagName("body")[0];
	var _phrase = _pageContent.innerHTML;
	var _source = "http://www.metta.lk";
	if (location.pathname.search("Thanissaro") > -1) _source = "https://www.dhammatalks.org";
	
	_phrase = "SOURCE : <a target=_blank href='" + _source + "'>" 
	+ _source + "</a></br>" + _phrase;
	
	_pageContent.innerHTML = _phrase.replace(_regExpPattern, match => `<a name=hl></a><span style="FONT-WEIGHT: underline; background-color:peachpuff; COLOR: green; FONT-STYLE: normal;">${match}</span>`);
	if (_keywords != "๛") {
		window.location.href = "#" + "hl";
	}
}
