<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Poems of the Elders: An Anthology from the Theragāthā &amp; Therīgāthā</title>
  <meta content="Ṭhānissaro Bhikkhu" name="Author"/>
  <link href="../Tools/Styles/Thag.css" rel="stylesheet" type="text/css"/>
  <script src="../../script.js" ></script>
 </head>

<body>
  <div id="thera-theri">
     <div id="introduction">
       <h1>Introduction</h1>

      <p>This is an anthology consisting of 94 poems from the Theragāthā (Poems of the Elder Monks) and 34 from the Therīgāthā (Poems of the Elder Nuns). These texts are, respectively, the eighth and ninth texts in the Khuddaka Nikāya, or Collection of Short Pieces, the last collection of the Sutta Piṭaka in the Pāli Canon.</p>

      <p>The Theragāthā contains a total of 264 poems, the Therīgāthā, 73, all attributed to early members of the monastic Saṅgha. Some of the poems are attributed to monks or nuns well-known from other parts of the Canon—such as Ānanda and Mahā Kassapa among the monks, and Mahāpajāpatī Gotamī and Uppalavaṇṇā among the nuns—whereas the majority are attributed to monks and nuns otherwise unknown.</p>

      <p>Both texts are landmarks in the history of world literature. The Therīgāthā is the earliest extant text depicting women’s spiritual experiences. The Theragāthā contains the earliest extant descriptions extolling the beauties, not of domesticated nature, but of nature where it’s wild.</p>

      <p>The poems in both compilations are arranged by ascending size, starting with chapters in which every poem consists of only one stanza, and working up numerically, chapter by chapter, to poems of many stanzas. The longest poem in the Theragāthā is 71 stanzas; the longest in the Therīgāthā, 75. Unlike the Dhammapada and Udāna, there is no overall aesthetic structure to either collection, although within a few of the chapters, such as the first chapter of the Theragāthā and the seventh of the Therīgāthā, poems of similar themes are grouped together.</p>

      <p>Because the poems are attributed to a wide variety of authors, it should come as no surprise that they differ widely in style, content, and artistic interest: Thus my choice to present an anthology of selected poems rather than a complete translation of either text. Some of the poems are autobiographical; some didactic. Some repeat verses attributed to the Buddha in other parts of the Canon, whereas others appear to be original compositions. Some are very simple and just barely poetic, whereas others are polished and artful, composed by people who obviously had a sophisticated literary background.</p>

      <p>The polished poems are among the most interesting, and to fully appreciate them it’s necessary to know something of the aesthetic theory that shaped their composition.</p>

      <h2 id="sigil_toc_id_1">Ancient Indian Aesthetics</h2>

      <p>The central concept in ancient Indian aesthetic theory was that every artistic text should have <em>rasa,</em> or “savor,” and the theory around savor was this: Artistic literature expressed states of emotion or states of mind called <em>bhāva.</em> The classic analysis of basic emotions listed eight: love (delight), humor, grief, anger, energy, fear, disgust, and astonishment. The reader or listener exposed to these presentations of emotion did not participate in them directly; rather, he/she savored them as an aesthetic experience at one remove from the emotion, and the savor—though related to the emotion—was somewhat different from it. The proof of this point was that some of the basic emotions were decidedly unpleasant, whereas the savor of the emotion was meant to be enjoyed.</p>

      <p>Each of the emotions had its corresponding savor, as follows:</p>

      <div class="dblock">
         <p>love — sensitive</p>

        <p>humor — comic</p>

        <p>grief — compassionate</p>

        <p>anger — furious</p>

        <p>energy — heroic</p>

        <p>fear — apprehensive</p>

        <p>disgust — horrific</p>

        <p>astonishment — marvelous</p>
      </div>

<!--end dblock-->       <p>Thus, for instance, a heroic character would feel energy, rather than heroism, but the reader would taste that energy as heroic. Characters in love would feel their love, but the reader/listener, in empathizing with their love, would taste that empathy as an experience of being sensitive.</p>

      <p>An ideal work of literary art was supposed to convey one dominant savor, but if it was long enough, it was expected, like a good meal, to offer many subsidiary savors as well. Some savors were believed to supplement one another naturally. The sensitive, for instance, was believed to blend well with the comic and the compassionate. The heroic often started with the apprehensive or furious, and tended to end with a touch of the marvelous. Other savors, however, worked at cross-purposes. The horrific, in particular, did not blend with the sensitive or the comic.</p>

      <p>All eight of the classic savors can be found in the poems of the Theragāthā (Thag) and Therīgāthā (Thig). <a href="Section0009.html#thig14">Thig 14</a>, for instance, begins with long passages conveying the sensitive savor, and ends with a jolt conveying an unusual and rule-breaking combination of the comic, horrific, heroic, and marvelous.<a href="Section0007.html#thag16.1">Thag 16:1</a> is a more classic example of the heroic and marvelous, whereas <a href="Section0004.html#thag1.104">Thag 1:104</a> conveys the marvelous on its own, and <a href="Section0005.html#thag2.24">Thag 2:24</a>, <a href="Section0005.html#thag2.37">2:37</a>, and <a href="Section0005.html#thag3.8">3:8</a> convey the purely heroic. <a href="Section0005.html#thag2.16">Thag 2:16</a> and <a href="Section0006.html#thag10.5">10:5</a> offer a savor of the horrific; <a href="Section0008.html#thig3.5">Thig 3:5</a> and <a href="Section0008.html#thig6.2">6:2</a>, the compassionate. <a href="Section0005.html#thag5.8">Thag 5:8</a> and <a href="Section0005.html#thag6.2">6:2</a> begin with the apprehensive before moving on to the heroic, and <a href="Section0005.html#thag2.47">Thag 2:47</a> conveys the furious by depicting a monk angry at his own mind.</p>

      <p>Unlike plays, however, which can convey savor through language, costumes, and the gestures of the actors, these poems, like all poems, convey savor solely through their use of language. Classical treatises devoted a great deal of space to discussions of how language could be used to convey different savors. And, while many of their recommendations had to do with the sound of the language—and are thus hard to convey when translating—some do survive translation.</p>

      <p>This is particularly true of two types of ornamentation: similes and a type of figure called a “lamp.” Lamps are a peculiarity of poetry in Indian languages, which are heavily inflected, a fact that allows a poet to use, say, one adjective to modify two different nouns, or one verb to function in two separate sentences. (The name of the figure derives from the idea that the two nouns radiate from the one adjective, or the two sentences from the one verb.) In English, the closest we have to this is parallelism combined with ellipsis. An example from the Theragāthā is <a href="Section0004.html#thag1.2">1:2</a>—</p>

      <div class="verse">
         <p>Calmed, restrained,</p>

        <p>giving counsel unruffled,</p>

        <p>he shakes off evil qualities—</p>

        <p class="v2">as the breeze,</p>

        <p class="v2">a leaf from a tree.</p>
      </div>

<!--end verse-->       <p>—where “shakes off” functions as the verb in both clauses, even though it is elided from the second. This is how I have rendered the lamps in most of the poems, although in a few cases, such as <a href="Section0004.html#thag1.111">Thag 1:111</a>, I have repeated the lamp word to emphasize its double role.</p>

      <p>Glancing through this anthology, you will quickly see that many of the poems succeed in conveying savor precisely because of their heavy use of similes and lamps. The lamps, through their concision, give a heightened flavor to the language without making it flowery. The similes flesh out with graphic images messages that without them would be abstract and dry.</p>

      <p>Other rhetorical features traditionally used to convey savor can also be found in the Theragāthā and Therīgāthā, such as: admonitions <em>(upadiṣṭa)</em> [<a href="Section0005.html#thag2.37">Thag 2:37</a>, <a href="Section0005.html#thag3.13">Thag 3:13</a>, <a href="Section0008.html#thig6.7">Thig 6:7</a>], distinctions <em>(viśeṣaṇa)</em> [<a href="Section0005.html#thag5.10">Thag 5:10</a>, <a href="Section0006.html#thag12.1">Thag 12:1</a>, <a href="Section0006.html#thag14.2">Thag 14:2</a>], encouragement <em>(protsāhana)</em> [<a href="Section0005.html#thag2.37">Thag 2:37</a>, <a href="Section0005.html#thag5.8">Thag 5:8</a>, <a href="Section0005.html#thag6.2">Thag 6:2</a>], examples <em>(dṛṣtānta)</em> [<a href="Section0008.html#thig3.4">Thig 3:4</a>], explanations of cause and effect <em>(hetu)</em> [<a href="Section0005.html#thag4.10">Thag 4:10</a>], illustrations <em>(udāharaṇa)</em> [<a href="Section0005.html#thag2.24">Thag 2:24</a>], rhetorical questions <em>(pṛcchā)</em> [<a href="Section0004.html#thag1.56">Thag 1:56</a>, <a href="Section0004.html#thag1.109">Thag 1:109</a>,<a href="Section0005.html#thag5.8">Thag 5:8</a>, <a href="Section0008.html#thig3.5">Thig 3:5</a>], prohibitions <em>(pratiṣedha)</em> [<a href="Section0005.html#thag2.47">Thag 2:47</a>; <a href="Section0008.html#thig9">Thig 9</a>], and praise <em>(guṇakīrtana)</em> [<a href="Section0006.html#thag15.2">Thag 15:2</a>, <a href="Section0008.html#thig6.6">Thig 6:6</a>].</p>

      <p>In all these formal respects, the poems in the Theragāthā and Therīgāthā do not differ markedly from those attributed to the Buddha in the Dhammapada, Udāna, and Itivuttaka. And although some of the poems in the Theragāthā and Therīgāthā, such as <a href="Section0004.html#thag1.100">Thag 1:100</a>, seem fairly formulaic in their attempts at inducing savor, others—in terms of Indian aesthetic theory—break new ground.</p>

      <p>One way in which they do this relates to the emotions some of the poems portray. As Indian aesthetic theory developed through the centuries, various writers argued for and against the addition of other savors to the standard list of eight. One of the prime candidates for a ninth savor was the <em>calmed,</em> the savor tasted when witnessing another person achieve peace. There are good reasons to believe that the first proponents of the calmed as a legitimate savor for literary works were Buddhist. For example, the great Buddhist poet, Aśvaghoṣa, who wrote epics and plays in the 1st century C.E., insisted that he was trying to lead his audience not to pleasure, but to calm. In this, he was echoing a sentiment expressed much earlier, in the Dhammapada, concerning the effect that Dhamma should have on its listeners:</p>

      <div class="verse">
         <p>Like a deep lake,</p>

        <p>clear, unruffled, &amp; calm:</p>

        <p>so the wise become clear,</p>

        <p class="v2">calm,</p>

        <p>on hearing words of the Dhamma. — <em>Dhp 82</em></p>
      </div>

<!--end verse-->       <div class="verse">
         <p>And better than chanting hundreds</p>

        <p>of meaningless verses is</p>

        <p class="v2">one</p>

        <p class="v2">Dhamma-saying</p>

        <p>that on hearing</p>

        <p>brings peace. <em>— Dhp 102</em></p>
      </div>

<!--end verse-->       <p>Both the Theragāthā and Therīgāthā contain many poems that achieve this effect of calm and peace by describing how the speaker attained the peace of awakening. A typical example is from Vimalā’s poem, <a href="Section0008.html#thig5.2">Thig 5:2</a>:</p>

      <div class="verse">
         <p>Today, wrapped in a double cloak,</p>

        <p class="v2">my head shaven,</p>

        <p class="v2">having wandered for alms,</p>

        <p>I sit at the foot of a tree</p>

        <p>and attain the state of no-thought.</p>

        <p>All ties—human &amp; divine—have been cut.</p>

        <p>Having cast off all</p>

        <p>effluents,</p>

        <p>cooled am I.&#160;&#160;&#160;&#160;&#160;&#160;&#160;Unbound.</p>
      </div>

<!--end verse-->       <p>Reading these lines, the reader savors some of Vimalā’s coolness and peace. Other poems in both compilations convey the same savor through other means. A prime example is Ambapālī’s poem, <a href="Section0009.html#thig13.1">Thig 13:1</a>, in which she graphically catalogs, part by part, how age has changed her body. After each part, however, she repeats the refrain: “The Truth-speaker’s word doesn’t change.” This refrain, which itself doesn’t change, has a calming effect, so that the reader tastes some of Ambapālī’s peace of mind as she views, from the bemused perspective of the timeless Dhamma, the changes wrought in her body by the ravages of time.</p>

      <p>These examples suggest that the poems of the Theragāthā and Therīgāthā may have been among the first conscious attempts to convey the calmed as a new savor, thus setting the stage for the further development of this savor in later centuries of Buddhist and even non-Buddhist poetry in India.</p>

      <p>Another way in which the poems of the Theragāthā and Therīgāthā broke new ground can be seen in how they subvert some of the traditions of ancient Indian aesthetic theory.</p>

      <p>A prime example is the rule-breaking poem mentioned above, <a href="Section0009.html#thig14">Thig 14</a>. Its story tells of a nun, Subhā, who is accosted by a libertine as she is going through a secluded patch of woods. He tries to persuade her to abandon her vows and become his wife. She, in turn, tries to show him the foolishness of his lust for her body. When asked what in her body he finds attractive, he focuses on the beauty of her eyes. So, after some further admonitions about the unattractive aspects of eyes, she plucks out one of her eyes and offers it to him. This, of course, makes very explicit the message that what he thought he desired is nothing worthy of desire. The libertine, shocked into his senses, asks for her forgiveness and allows her to go on her way. She returns to the Buddha, and when she gazes at him, her eye is restored.</p>

      <p>From an aesthetic point of view, two features of the poem are especially striking. First, the libertine is given some of the most beautiful lines in Pāli poetry. But this is a setup. The lines are obviously intended to create a savor of the sensitive, but this savor will then be drastically undercut by the horrific savor induced when Subhā plucks out her eye. The libertine’s skill with words is thus exposed as the skill of a fool. In this way, the poem conveys the message that when people deny the allure of sensuality, it’s not necessarily because they are too dull to have developed refined tastes. They, too, are able to appreciate the beauties of language well enough to compose alluring lines. So their rejection of sensuality is not a sign of lack of sophistication. Instead, it’s a sign that they have gone beyond sophistication to something higher.</p>

      <p>The second striking feature is the combination of the comic and the horrific at the conclusion to the poem. Subhā’s act of removing her eye is obviously disgusting, yet at the same time it’s hard not to imagine her laughing at her own bravado in carrying it out. “You want it?” she seems to say, “all right, you can have it.” This combination of the comic and the horrific broke one of the classic rules of Indian aesthetic theory, a fact that underscores the complete freedom with which Subhā is acting. Not only is she so free of attachment to sensuality that she can play a trick like this on the libertine, she is also free enough to break long-established literary conventions.</p>

      <p>Another poem that breaks with ancient Indian aesthetic theory is <a href="Section0006.html#thag14.1">Thag 14:1</a>. This poem depicts Ven. Revata’s last words before entering total unbinding, and in so doing it breaks with an ancient Indian taboo against presenting a character’s death. The standard procedure in plays, when dealing with a death, was to report it as happening off-stage. The dying character was never presented saying his last words. The reason that the compilers of the Theragāthā felt free to break with this tradition may be related to the fact that the ability to attain arahantship gave a new meaning to death. Instead of being an occasion for fear or grief, an arahant’s death was peaceful. A poem presenting an arahant’s last words would thus convey, not apprehension or compassion, but a savor of calm.</p>

      <p>The poems of the Theragāthā and Therīgāthā break new ground not only in terms of their aesthetic form but also in terms of their subject matter. As noted above, the Therīgāthā is the earliest extant text to convey accounts of women’s spiritual experiences, ostensibly in their own words. This in itself is quite countercultural, but doubly countercultural are the poems in which women report the sense of freedom that comes from contemplating the unattractiveness of the body (see, for instance, <a href="Section0008.html#thig2.4">Thig 2:4</a>, <a href="Section0008.html#thig5.4">5:4</a>, and <a href="Section0009.html#thig14">14</a>) to counteract pride and lust. Because ancient Indian culture, like so many human cultures, taught women to identify strongly with their bodies and to judge themselves by how attractive their bodies appeared, these poems make an important point: The best way not to suffer over the issue of your body’s appearance is not to work at cultivating a continually positive image of that appearance. It’s to develop dispassion toward the issue of appearance entirely, and to find a happiness not based on things inconstant and subject to change. At the same time, because the contemplation of unattractiveness is regarded as a painful practice (AN 4:163), the accounts of women who have succeeded at this practice convey a savor of the heroic.</p>

      <p>As for the Theragāthā, it breaks new ground in terms of subject matter with its poems extolling the beauties of the wilderness. Ancient Indian culture, like all pre-modern cultures that had developed past the hunter-gatherer stage, tended to view the wilderness with suspicion and fear. But the Buddhist monks had essentially returned to an economy very similar to that of hunter-gatherers—hunting, in the words of <a href="Section0009.html#thig13.2">Thig 13:2</a>, only what is already cooked. Thus they had learned, like earlier hunter-gatherers, to view the wilderness as home, an ideal place to hunt for the deathless. Thus there is good reason for the many poems in the Theragāthā dealing with the beauty of the wilderness—<a href="Section0007.html#thag18">Thag 18</a> is the primary example, but <a href="Section0004.html#thag1.13">1:13</a>, <a href="Section0004.html#thag1.22">1:22</a>, <a href="Section0004.html#thag1.41">1:41</a>, <a href="Section0004.html#thag1.110">1:110</a>, <a href="Section0004.html#thag1.113">1:113</a>, <a href="Section0006.html#thag10.2">10:2</a>, and 11 fall into this category as well. The point of these poems is not that beauty of this sort is an end in itself, but that the wilderness provides an ideal place to refresh the mind in its quest for a higher happiness.</p>

      <p>Now, it’s true that other poems in the Theragāthā, such as <a href="Section0004.html#thag1.31">Thag 1:31</a>, <a href="Section0005.html#thag3.5">3:5</a>, <a href="Section0005.html#thag3.8">3:8</a>, and <a href="Section0005.html#thag5.8">5:8</a>, detail the hardships of living in the wilderness, but these poems are not meant to discourage their readers from taking up the wilderness life. On the contrary, they appeal to the reader’s desire to take up a life with a heroic dimension. In this way, the Theragāthā makes the wilderness life of a monk attractive both aesthetically and energetically. Thus these poems are good inducements for seeking seclusion and trying to gain the benefits of practicing there.</p>

      <p>And in these ways, by breaking new ground in terms of subject matter, both the Theragāthā and Therīgāthā at the same time provide new standards of heroism for ancient Indian culture—and for world culture at large.</p>

      <h2 id="sigil_toc_id_2">Authorship &amp; Authenticity</h2>

      <p>Just who composed these poems, and put so much art into them, is a matter of conjecture. There is also no way of knowing who compiled them in their present form or when. There are several reasons to believe that many parts of both the Theragāthā and Therīgāthā were composed quite late, at least two centuries after the Buddha’s passing away. <a href="Section0006.html#thag10.2">Thag 10:2</a>, for instance, is attributed to King Asoka’s younger brother, who postdated the Buddha by a century or two. Also, both the Theragāthā and Therīgāthā are placed in the Khuddaka after two other compositions generally regarded as late—the Vimānavatthu and Petavatthu—which suggests that they too, even though they may contain earlier material, were compiled at a relatively late date.</p>

      <p>Some scholars have proposed that the Theragāthā and Therīgāthā were compiled as part of the movement to provide early Buddhism with dramatic stage pieces as a way of making the teaching attractive to the masses: a trend that culminated in later centuries in a thriving Buddhist theatre as Buddhism became an established, wealthy religion. In formal terms, many of the poems in the Theragāthā and Therīgāthā would seem to bear this theory out. <a href="Section0006.html#thag10.2">Thag 10:2</a>, <a href="Section0006.html#thag11">11</a>, and <a href="Section0006.html#thag14.1">14:1</a>, for instance, read like dramatic monologues;<a href="Section0007.html#thag16.1">Thag 16:1</a>, <a href="Section0008.html#thig9">Thig 9</a>, <a href="Section0009.html#thig12">Thig 12</a>, and <a href="Section0009.html#thig13.2">Thig 13:2</a>, like dramatic dialogues. Three poems—<a href="Section0008.html#thig7.2">Thig 7:2</a>, <a href="Section0008.html#thig7.3">7:3</a>, and <a href="Section0008.html#thig8">8</a>—have parallels in another part of the Canon (SN 5), and one of the ways they differ from those parallels is that each is introduced with a stanza that would serve well as a dramatic introduction on stage.</p>

      <p>Another dramatic element in both the Theragāthā and Therīgāthā that differs from the earlier suttas is that in many of the autobiographical poems, the speaker proclaims his/her awakening in these terms: “Cooled am I”—or “calmed am I”— “unbound.” (See, for instance, <a href="Section0005.html#thag4.10">Thag 4:10</a> and <a href="Section0008.html#thig5.2">Thig 5:2</a>.) These statements would make a dramatic impact if presented on stage. But, in the context of the early teachings, such an announcement, with its reference to “I,” was proof that the speaker was not really awakened. See for instance, the Buddha’s statement at MN 102, referring to a person announcing, “I am at peace, I am unbound, I am without clinging”: “The fact that he envisions that ‘I am at peace, I am unbound, I am without clinging!’—that in itself points to his clinging.” Or this statement in AN 6:49 about the proper way to proclaim gnosis, or the knowledge of full awakening: “Monks, this is how clansmen declare gnosis. The meaning is stated, but without mention of self.” This is one way in which the dramatic form of the poems distorts an important point in the training of the mind.</p>

      <p>The predominance of drama over Dhamma in the Theragāthā and Therīgāthā can also be seen in the way some of their poems treat an issue central to both compilations: attachment to the body. Many poems—such as <a href="Section0008.html#thig5.4">Thig 5:4</a>, <a href="Section0005.html#thag5.1">Thag 5:1</a>, and <a href="Section0005.html#thag7.1">Thag 7:1</a>—relate how the speakers gained full awakening on abandoning precisely this attachment, but the description of the process in each case leaves out many details that other texts show are crucial. To begin with, in the poems of this category in the Theragāthā, full awakening comes with overcoming attachment to the body of the opposite sex. Only in the Therīgāthā poems does awakening come with overcoming attachment to one’s own body. In this respect, the Therīgāthā is closer than the Theragāthā to the sutta accounts of what is required for overcoming this attachment, because, as AN 7:48 points out, attraction to the opposite sex begins with attraction to one’s own body. So in this way, the Theragāthā poems leave out an important step when describing how attachment is overcome.</p>

      <p>However, poems of this category in both compilations leave out an even more important step in describing how the abandoning of attachment leads to awakening. In each case, these poems describe the awakening that comes with abandoning passion for the body as total. But suttas detailing the fetters abandoned with the four stages of awakening, such as AN 10:13 and MN 118, indicate that simply overcoming sensual passion is a mark, not of the fourth state, total awakening, but only of the third stage, non-return. Some of the poems in the Theragāthā dealing with this topic, such as <a href="Section0006.html#thag10.5">Thag 10:5</a>, do indicate that total awakening requires more than abandoning attachment to the body, but <a href="Section0008.html#thig5.4">Thig 5:4</a>, <a href="Section0005.html#thag5.1">Thag 5:1</a>, and <a href="Section0005.html#thag7.1">Thag 7:1</a>, if read on their own, could create the impression that nothing more is needed.</p>

      <p>Now, if these poems were intended for dramatic presentation, it’s easy to understand why they give such a compressed account of how awakening is achieved: More detailed accounts would deprive the poems of their dramatic effect. But the effect has its price, in giving a distorted sense of the practice.</p>

      <p>For these reasons, it is possible that the existence of the Theragāthā and Therīgāthā is related to a complaint, voiced in some of the other suttas, that with the passage of time people will become less interested in the Buddha’s teachings and instead will pay more attention to “literary works—the works of poets, artful in sound, artful in rhetoric… words of disciples” (AN 5:79). Many of the poems in the Theragāthā and Therīgāthā fit this latter description precisely.</p>

      <p>All of this means that for a person interested in the practice of the Dhamma, the Theragāthā and Therīgāthā should be read with caution and care. The stories told in their poems, and the people they portray, are inspiring and attractive, but their example may not be the best to follow in every respect, and the Dhamma they teach has to be checked against more reliable sources.</p>

      <p>These poems do mention many of the standard doctrines of early Buddhism, such as the triple refuge (<a href="Section0009.html#thig13.2">Thig 13:2</a>), kamma (<a href="Section0009.html#thig12">Thig 12</a>), the four noble truths (<a href="Section0008.html#thig7.3">Thig 7:3</a>), the eightfold path (<a href="Section0008.html#thig6.6">Thig 6:6</a>), the establishings of mindfulness (<a href="Section0004.html#thag1.100">Thag 1:100</a>), the ten fetters (<a href="Section0008.html#thig6.7">Thig 6:7</a>), the five hindrances (<a href="Section0005.html#thag2.26">Thag 2:26</a>), the five aggregates (<a href="Section0004.html#thag1.23">Thag 1:23</a>), the practice of goodwill, or <em>mettā</em> (<a href="Section0009.html#thig14">Thig 14</a>), and the practice of <em>jhāna,</em> or meditative absorption (<a href="Section0004.html#thag1.41">Thag 1:41</a>, <a href="Section0004.html#thag1.43">1:43</a>, <a href="Section0004.html#thag1.85">1:85</a>, <a href="Section0004.html#thag1.119">1:119</a>). They also employ the concepts of effluent <em>(āsava)</em> and kamma in their strictly Buddhist sense (see <a href="Section0004.html#thag1.100">Thag 1:100</a>, note 1, and <a href="Section0007.html#thag16.8">Thag 16:8</a>, note 4), and <a href="Section0005.html#thag3.14">Thag 3:14</a> provides a quick tour of the Buddhist cosmos. However, none of these doctrines are discussed in any detail. For discussions detailed enough to be of practical help, you have to look elsewhere in the Canon.</p>

      <p>Like the Udāna, the Theragāthā and Therīgāthā seem concerned less with explaining specific Dhamma teachings and more with portraying early Buddhist values. In particular, the values listed in two suttas from the Aṅguttara Nikaya—AN 7:80 and AN 8:53—are well-represented in the poems of both collections.</p>

      <p>Here, for example, are some poems illustrating the values listed in AN 8:53:</p>

      <div class="dblock">
         <p>dispassion — <em><a href="Section0004.html#thag1.39">Thag 1:39</a>; <a href="Section0008.html#thig5.4">Thig 5:4</a>; <a href="Section0009.html#thig13.5">Thig 13:5</a></em></p>

        <p>being unfettered — <em><a href="Section0008.html#thig1.11">Thig 1:11</a>; <a href="Section0008.html#thig2.3">Thig 2:3</a>; <a href="Section0008.html#thig6.7">Thig 6:7</a></em></p>

        <p>shedding — <em><a href="Section0005.html#thag6.9">Thag 6:9</a>; <a href="Section0008.html#thig5.2">Thig 5:2</a></em></p>

        <p>modesty — <em><a href="Section0005.html#thag6.10">Thag 6:10</a></em></p>

        <p>contentment — <em><a href="Section0007.html#thag16.7">Thag 16:7</a>;<a href="Section0007.html#thag18">Thag 18</a></em></p>

        <p>reclusiveness — <em><a href="Section0005.html#thag3.8">Thag 3:8</a>;<a href="Section0007.html#thag18">Thag 18</a></em></p>

        <p>aroused persistence — <em><a href="Section0005.html#thag2.24">Thag 2:24</a>; <a href="Section0005.html#thag3.5">Thag 3:5</a></em></p>

        <p>being unburdensome — <em><a href="Section0009.html#thig13.2">Thig 13:2</a></em></p>
      </div>

<!--end dblock-->       <p>And here are some poems illustrating the values listed in AN 7:80:</p>

      <div class="dblock">
         <p>disenchantment — <em><a href="Section0005.html#thag5.1">Thag 5:1</a>; <a href="Section0005.html#thag6.6">Thag 6:6</a>; <a href="Section0005.html#thag7.1">Thag 7:1</a>; <a href="Section0006.html#thag10.5">Thag 10:5</a></em></p>

        <p>dispassion — <em><a href="Section0004.html#thag1.18">Thag 1:18</a>; <a href="Section0005.html#thag2.30">Thag 2:30</a></em></p>

        <p>cessation — <em><a href="Section0008.html#thig6.6">Thig 6:6</a>; <a href="Section0005.html#thag3.15">Thag 3:15</a></em></p>

        <p>stilling — <em><a href="Section0008.html#thig7.2">Thig 7:2</a></em></p>

        <p>direct knowledge — <em><a href="Section0005.html#thag3.14">Thag 3:14</a>; <a href="Section0008.html#thig7.3">Thig 7:3</a>; <a href="Section0008.html#thig9">Thig 9</a></em></p>

        <p>self-awakening — <em><a href="Section0006.html#thag12.2">Thag 12:2</a>; <a href="Section0006.html#thag14.2">Thag 14:2</a></em></p>

        <p>unbinding — <em><a href="Section0004.html#thag1.32">Thag 1:32</a>; <a href="Section0006.html#thag14.1">Thag 14:1</a>; <a href="Section0008.html#thig5.10">Thig 5:10</a></em></p>
      </div>

<!--end dblock-->       <p>Fortunately, given the uncertain provenance of the poems in the Theragāthā and Therīgāthā, only a few attempt to discuss high-level Dhamma in any detail. <a href="Section0006.html#thag15.2">Thag 15:2</a> is one of the exceptions, as it contains a rare image to illustrate why the arahant, prior to death, is said to experience unbinding “with fuel remaining,” and why, by extension, the experience of unbinding after death is said to have “no fuel remaining.” However, the same poem appears in AN 6:43, indicating the Theragāthā here is not deviating from a more reliable source.</p>

      <p>There is also only one teaching appearing in these texts that doesn’t appear elsewhere in the early suttas: <a href="Section0007.html#thag16.7">Thag 16:7</a> contains the only complete list of all thirteen ascetic <em>(dhutaṅga)</em> practices to be found anywhere in the Canon. But this is not a point of high-level theory, and more a matter of everyday practice that the reader can easily test for him or herself.</p>

      <p>So these texts seem meant to be read, not for detailed information about the path of practice, but for the savor with which they make the practice attractive and the encouragement they give for taking up the practice yourself.</p>

      <h2 id="sigil_toc_id_3">Recollection of the Saṅgha</h2>

      <p>The best way to use these poems is to read them as aids in the meditative exercise called recollection of the Saṅgha <em>(saṅghānussati).</em> And they aid in this exercise in two ways.</p>

      <p>The first way relates to the fact that, elsewhere in the Canon, the description of this practice is fairly abstract and dry:</p>

      <div class="iblock">
         <p>“There is the case where the disciple of the noble ones recollects the Saṅgha, thus: ‘The Saṅgha of the Blessed One’s disciples who have practiced well… who have practiced straight-forwardly… who have practiced methodically… who have practiced masterfully—in other words, the four types (of noble disciples) when taken as pairs, the eight when taken as individual types—they are the Saṅgha of the Blessed One’s disciples: worthy of gifts, worthy of hospitality, worthy of offerings, worthy of respect, the incomparable field of merit for the world.’” <em>— AN 3:71</em></p>
      </div>

<!--end block-->       <p>The narratives in both the Theragāthā and Therīgāthā add flesh and blood to this contemplation, giving graphic examples of what it means to practice well and why those who practice well are worthy of respect. This point applies both to the cases where the narratives tell of monks and nuns who face danger with nobility and calm already firmly in place (such as <a href="Section0007.html#thag16.1">Thag 16:1</a> and <a href="Section0009.html#thig14">Thig 14</a>), and in those where the monks and nuns have to overcome great weakness, misfortune, or discouragement to achieve final awakening (such as <a href="Section0005.html#thag6.6">Thag 6:6</a> and <a href="Section0009.html#thig10">Thig 10</a>).</p>

      <p>These latter examples, in particular, aid in a strategy that Ven. Ānanda called relying on conceit to abandon conceit:</p>

      <div class="iblock">
         <p>“There is the case, sister, where a monk hears, ‘The monk named such-and-such, they say, through the ending of the effluents, has entered &amp; remains in the effluent-free awareness-release &amp; discernment-release, having directly known &amp; realized them for himself right in the here &amp; now.’ The thought occurs to him, ‘The monk named such-&amp;-such, they say, through the ending of the effluents, has entered &amp; remains in the effluent-free awareness-release &amp; discernment-release, having directly known &amp; realized them for himself right in the here &amp; now. Then why not me?’ Then, at a later time, he abandons conceit, having relied on conceit.” <em>— AN 4:159</em></p>
      </div>

<!--end block-->       <p>Seeing the difficulties that others have overcome before reaching awakening makes it easier to imagine that you, too, can overcome your personal difficulties and reach awakening as well. If they can do it, why not you?</p>

      <p>The second way in which the poems of the Theragāthā and Therīgāthā aid in the practice of recollecting the Saṅgha comes in their own examples of monks and nuns who engage in this practice themselves, showing the purposes for which it’s useful.</p>

      <p>Elsewhere in the suttas, the recollection of the Saṅgha is said to serve three purposes: AN 3:71 says that it cleanses the mind, gives rise to joy, and helps one to abandon defilements. SN 11:3 says that it helps to overcome fear when one is practicing alone in an empty dwelling or in the wilderness. SN 47:10 points out that if one has trouble staying with any of the four establishings of mindfulness, one can focus on an inspiring theme—and the recollection of the Saṅgha counts as an inspiring theme—to wake up the sluggish mind, gather the scattered mind, and give rise to rapture and calm. Once the mind has gained this rapture and calm, it can drop the inspiring theme, and it will be in a state of concentration devoid of directed thought and evaluation: apparently, the second jhāna.</p>

      <p>The Theragāthā presents two additional rewards for the practice of recollecting the Saṅgha: <a href="Section0005.html#thag6.2">Thag 6:2</a> gives an example of a monk who, gaining no alms, nourishes himself with the rapture coming from recollecting the Saṅgha. <a href="Section0005.html#thag5.8">Thag 5:8</a> portrays a monk who, alone in the wilderness, has fallen sick. He gains strength of heart not to retreat from the wilderness and instead to use the Dhamma to cure his illness with this reflection:</p>

      <div class="verse">
         <p>Reflecting on those who are resolute,</p>

        <p>their persistence aroused,</p>

        <p>constantly firm in their effort,</p>

        <p>united in concord,</p>

        <p class="v2">I’ll stay in the grove.</p>
      </div>

<!--end verse-->       <p>Although much of the initial appeal of the Theragāthā and Therīgāthā lies in the artistry of the poems, it’s when they yield this sort of reflection that they prove most useful in the long run.</p>
    </div>

<!--end introduction-->
  </div>
</body>
</html>