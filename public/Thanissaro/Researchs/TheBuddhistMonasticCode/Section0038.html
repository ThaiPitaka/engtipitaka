<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:awml="http://www.abisource.com/2004/xhtml-awml/">
<head>
  <!-- ======================================================= -->
  <!-- Created by AbiWord, a free, Open Source wordprocessor.  -->
  <!-- For more information visit http://www.abisource.com.    -->
  <!-- ======================================================= -->

  <title>The Buddhist Monastic Code II: The Khandhaka Rules</title>
  <meta content="The Buddhist Monastic Code, Volumes I &amp; II" name="Title" />
  <meta content="Ṭhanissaro Bhikkhu" name="Author" />
  <link href="../../Styles/TheBuddhistMonasticCode.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <div id="introduction2">
    <p class="chapterheading">INTRODUCTION</p><!--end chapterheading-->

    <h2>The Khandhakas</h2>

    <p><span class="allcaps">The Khandhakas</span>—literally, “Collections”—form the second major portion of the Vinaya Piṭaka, following the Sutta Vibhaṅga and preceding the Parivāra. There are 22 Khandhakas in all, divided into two groups: the Mahāvagga (Mv.), or Great Chapter, composed of ten Khandhakas; and the Cullavagga (Cv.), or Lesser Chapter, composed of twelve. Each Khandhaka is loosely organized around a major topic, with minor topics inserted in a fairly haphazard fashion. The major topics are these:</p>

    <div class="blockl">
      <p>Mv.I—Ordination</p>

      <p>Mv.II—Uposatha</p>

      <p>Mv.III—Rains-residence</p>

      <p>Mv.IV—Invitation</p>

      <p>Mv.V—Footwear</p>

      <p>Mv.VI—Medicine</p>

      <p>Mv.VII—Kaṭhina</p>

      <p>Mv.VIII—Robe-cloth</p>

      <p>Mv.IX—Principles for Community Transactions</p>

      <p>Mv.X—Unanimity in the Community</p>
    </div>

    <div class="blockl">
      <p>Cv.I—Disciplinary Transactions</p>

      <p>Cv.II—Penance &amp; Probation</p>

      <p>Cv.III—Imposing Penance &amp; Probation</p>

      <p>Cv.IV—Settling Issues</p>

      <p>Cv.V—Miscellany</p>

      <p>Cv.VI—Lodgings</p>

      <p>Cv.VII—Schism</p>

      <p>Cv.VIII—Protocols</p>

      <p>Cv.IX—Canceling the Pāṭimokkha</p>

      <p>Cv.X—Bhikkhunīs</p>

      <p>Cv.XI—The First Council</p>

      <p>Cv.XII—The Second Council</p>
    </div>

    <p>Aside from their opening and closing narratives, there seems little overall plan to the Khandhakas’ arrangement. The first Khandhaka opens with a narrative of the events beginning with the Buddha’s Awakening; continuing through the conversion of his two major disciples, Vens. Sāriputta and Moggallāna; and concluding with the Buddha’s authorization of the Saṅgha to accept new members into its fold.</p>

    <p>The account of the Awakening and the Buddha’s success in leading others to Awakening establishes his legitimacy as a lawgiver, the source of all the rules the Khandhakas contain.</p>

    <p>The story of the conversion of the two major disciples establishes two principles: The awakening of the Dhamma Eye in Ven. Sāriputta shows that the path to Awakening can be successfully taught outside of the Buddha’s presence, using words other than the Buddha’s own; the awakening of the Dhamma Eye in Ven. Moggallāna shows that the path to Awakening can be successfully taught by disciples who have not even met the Buddha. These two principles indicate that the path to Awakening did not necessarily depend on personal contact with the Buddha, and that it can thus be legitimately and effectively taught in times and places such as ours, far removed from his physical presence.</p>

    <p>The story of the Buddha’s authorizing the Saṅgha to accept new members establishes the legitimacy of each new bhikkhu accepted in line with the prescribed pattern. The Saṅgha that has accepted him owes its status to an allowance coming from the Buddha, and his preceptor belongs to a lineage stretching back to the Buddha himself.</p>

    <p>In this way, the opening narratives establish the legitimacy of the Bhikkhu Saṅgha and of the training for the bhikkhus as embodied in the Khandhakas and the Vinaya as a whole.</p>

    <p>As for the closing narratives, both the Mahāvagga and Cullavagga end with accounts that juxtapose misbehaving city bhikkhus with well-behaved wilderness bhikkhus. The placement of these accounts seems intended to make a point: that the survival of the Dhamma-Vinaya will depend on bhikkhus who practice in the wilderness. This is in keeping with a passage from the discourses (<a class="outlink" href="https://dhammatalks.org/suttas/AN/AN7_21.html">AN 7:21</a>) that “as long as the bhikkhus see their own benefit in wilderness dwellings, their growth can be expected, not their decline.”</p>

    <p>Between these framing narratives, however, the Khandhakas seem randomly ordered, and the internal arrangement of individual Khandhakas is often even more haphazard. This lack of clear organization creates a problem for any bhikkhu who wants to train by the Khandhaka rules, as rules related in practice are often scattered in widely different spots of the text. The purpose of this volume is to bring related rules together in a coherent way that will make them easier to understand and put into practice.</p>

    <h3 id="sigil_toc_id_49">Format</h3>

    <p>Topically, the rules in the Khandhakas fall into three major categories, dealing with (1) general issues, (2) Community transactions, and (3) relations between bhikkhus and their co-religionists, i.e., bhikkhunīs and novices. To reflect these categories, this volume is organized into the same three parts. Each part is further divided into chapters, with each chapter devoted to a particular topic. With one exception (<a class="bmc2link" href="Section0048.html#BMC2chapter9">Chapter 9</a>), each chapter falls into two sections: translations of the rules related to that topic, preceded by an explanatory discussion. The discussion provides an overview of the topic of the chapter, explaining the individual rules related to the topic, at the same time showing the relationships among the rules. Its purpose is to provide an understanding of the rules sufficient for any bhikkhu who wants to live by them. The rule translations are included to show the raw material from the Canon on which the discussion is based. As for <a class="bmc2link" href="Section0048.html#BMC2chapter9">Chapter 9</a>, its topic—the protocols—is contained in detailed rules requiring little discussion, so its format is that of rule translations with brief annotations.</p>

    <h3 id="sigil_toc_id_50">Rules</h3>

    <p>Formally, the rules in the Khandhakas are of three sorts: prohibitions, allowances, and directives. Most of the directives are <em>de facto</em> prohibitions: If a bhikkhu does not do as directed, he incurs a penalty. However, some of the directives—such as the protocols (<a class="bmc2link" href="Section0048.html#BMC2chapter9">Chapter 9</a>) and the directions on how not to wear one’s robes—give more room for leeway. If a bhikkhu has good reason to deviate from them, he incurs no penalty in doing so. The penalty applies only when he deviates from them out of disrespect. Throughout this volume, the reader should assume all directives to be <em>de facto</em> prohibitions unless otherwise noted.</p>

    <p>In terms of their seriousness, the vast majority of rules in the Khandhakas involve dukkaṭas (offenses of wrong doing), with a small number of thullaccayas (grave offenses) scattered among them. The text makes occasional references to the rules in the Pāṭimokkha, and—as anyone who has read <a href="Cover.html">BMC1</a> will have noted—these references play an important role in determining the range of those rules. In this volume, where the seriousness of a particular offense is not mentioned, the reader should assume it to be a dukkaṭa. Other grades of offenses will be specifically noted.</p>

    <p>In most cases, the citations in the Rules section of each chapter are straight translations from the Canon. However, there are passages—especially among the directives—where a straight translation would prove unduly long and repetitive, adding nothing to the discussion, so I have simply given a synopsis of the main points in the passage. For procedures and transaction statements <em>(kamma-vācā)</em> used in Community transactions <em>(saṅgha-kamma),</em> I have simply noted the chapter and section number where these passages can be found in <em>The Book of Discipline</em> (BD). Frequently-used transaction statements are provided in the Appendices. Passages where my translation differs from that in BD are marked with a (§).</p>

    <p>A few of the passages in the Rules sections are not mentioned in their respective discussions. In most cases, this is because these rules are discussed elsewhere, either in <a href="Cover.html">BMC1</a> or in this volume. However, there are also cases where a particular rule or transaction developed over time. For instance, <!--<a href="https://dhammatalks.org/vinaya/Mv/MvI.html" class="outlink">-->Mv.I<!--</a>--> shows that the procedures for Acceptance—the Community transaction whereby new members are admitted to the Saṅgha—underwent many changes in response to incidents before achieving their final form. In cases like this, the text-locations of the earlier forms of the rules and transaction patterns are cited in the Rules section, but only the final forms are translated and discussed. Rules in Cv.X that affect only the bhikkhunīs and not the bhikkhus are best understood in the context of the Bhikkhunī Pāṭimokkha, and so are not translated or discussed here.</p>

    <h3 id="sigil_toc_id_51">Discussions</h3>

    <p>Unlike its treatment of the Pāṭimokkha rules, the Canon does not provide word-commentaries for the Khandhaka rules. And, although it does provide an origin story for each rule, there are unfortunately very few cases where the story actually helps to explain the rule. In some cases, the origin story is terse, adding little information to what is in the rule. In others, the origin story is extremely long (the English translation of the origin story to the first rule in <!--<a href="https://dhammatalks.org/vinaya/Mv/MvI.html" class="outlink">-->Mv.I<!--</a>--> takes up 51 pages in BD) and yet has very little to do with the rule it introduces. For instance, the origin story to the rule permitting bhikkhus to accept gifts of robe-cloth from lay donors tells the life story of Jīvaka Komārabhacca, the first lay person to give such a gift to the Buddha. Although Jīvaka’s story is fascinating in and of itself, providing many interesting insights into attitudes in the early Saṅgha, it is largely irrelevant to the rule at hand.</p>

    <p>Thus the primary way the discussions use the Canon in helping to explain the rules is by placing each rule in connection to those related to it. From this placement one may gain a picture of how the rules fit into a coherent whole.</p>

    <p>Given this picture, it is then possible to add explanatory material from other sources. These sources include Buddhaghosa’s Commentary to the Vinaya (the <em>Samanta-pāsādikā),</em> two sub-commentaries (Sāriputta’s <em>Sārattha-dīpanī</em> and Kassapa’s <em>Vimati-vinodanī),</em> two Thai Vinaya guides (the <em>Pubbasikkhā-vaṇṇanā</em> and Prince Vajirañāṇa’s <em>Vinaya-mukha),</em> and—occasionally—oral traditions concerning the rules. Very few scholars have written on the Khandhakas of other early Buddhist schools, so references in this volume to other early Buddhist canons are rare. As in <a href="Cover.html">BMC1</a>, I give preference to the earlier Theravādin sources when these conflict with later ones, but I do so with a strong sense of respect for the later sources, and without implying that my interpretation of the Canon is the only one valid. There is always a danger in being too independent in interpreting the tradition, in that strongly held opinions can lead to disharmony in the Community. Thus, even in instances where I think the later sources misunderstand the Canon, I have tried to give a faithful account of their positions—sometimes in great detail—so that those who wish to take those sources as their authority, or who wish to live harmoniously in Communities that do, may still use this book as a guide.</p>

    <p>And—again, as in <a href="Cover.html">BMC1</a>—I have tried to include whatever seems most worth knowing for the bhikkhu who aims at using the Khandhaka rules to foster the qualities of discipline in his life—so as to help train his mind and live in peace with his fellow bhikkhus—and for anyone who wants to support and encourage the bhikkhus in that aim.</p>
  </div><!--end introduction2-->
</body>
</html>
