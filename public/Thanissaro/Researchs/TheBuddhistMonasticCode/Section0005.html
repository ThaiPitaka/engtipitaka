<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:awml="http://www.abisource.com/2004/xhtml-awml/">
<head>
  <!-- ======================================================= -->
  <!-- Created by AbiWord, a free, Open Source wordprocessor.  -->
  <!-- For more information visit http://www.abisource.com.    -->
  <!-- ======================================================= -->

  <title>The Buddhist Monastic Code I: The Pāṭimokkha Rules</title>
  <meta content="The Buddhist Monastic Code, Volumes I &amp; II" name="Title" />
  <meta content="Ṭhanissaro Bhikkhu" name="Author" />
  <link href="../../Styles/TheBuddhistMonasticCode.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <div id="preface1">
    <h2>Preface</h2>

    <p><span class="allcaps">This volume</span> is the first in a two-volume book that attempts to give an organized, detailed account of the Vinaya training rules and the traditions that have grown up around them. The Pāṭimokkha training rules as explained in the Sutta Vibhaṅga are the topic of the first volume; the rules found in the Khandhakas, the topic of the second. The book as a whole is aimed primarily at those whose lives are affected by the rules—bhikkhus who live by them, and other people who have dealings with the bhikkhus—so that they will be able to find gathered in one location as much essential information as possible on just what the rules do and do not entail. Students of Early Buddhism, Theravādin history, or contemporary Theravādin issues should also find this book interesting, as should anyone who is serious about the practice of the Dhamma and wants to see how the Buddha worked out the ramifications of Dhamma practice in daily life.</p>

    <p>The amount of information offered here is both the book’s strength and its weakness. On the one hand, it encompasses material that in some cases is otherwise unavailable in English or even in romanized Pali, and should be sufficient to serve as a life-long companion to any bhikkhu who seriously wants to benefit from the precise and thorough training the rules have to offer. On the other hand, the sheer size of the book and the mass of details to be remembered might prove daunting or discouraging to anyone just embarking on the bhikkhu’s life.</p>

    <p>To overcome this drawback, I have tried to organize the material in as clear-cut a manner as possible. In particular, in volume one I have analyzed each rule into its component factors so as to show not only the rule’s precise range but also how it connects to the general pattern of mindfully analyzing one’s own actions in terms of such factors as intention, perception, object, effort, and result—a system that plays an important role in the training of the mind. In volume two, I have gathered rules by subject so as to give a clear sense of how rules scattered randomly in the texts actually relate to one another in a coherent way.</p>

    <p>Secondly, in volume one I have provided short summaries for the Pāṭimokkha rules and have gathered them, organized by topic, in the Rule Index at the back of the volume. <em>If you are new to the subject of Buddhist monastic discipline, I suggest that you read the Rule Index first,</em> to grasp the gist of the main rules and their relationship to the Buddhist path, before going on to the more detailed discussions in the body of the book. This should help you keep the general purpose of the rules in mind, and keep you from getting lost in the mass of details.</p>

    <p>I am indebted to the many people who helped directly and indirectly in the writing of this book. Phra Ajaan Fuang Jotiko (Phra Khru Ñāṇavisitth) and Phra Ajaan Thawng Candasiri (Phra Ñāṇavisitth), my first teachers in Vinaya, gave me a thorough grounding in the subject. Ven. Brahmavaṁso Bhikkhu gave many hours of his time to writing detailed criticisms of early versions of the manuscript for the first edition of volume one, forcing me to deepen my knowledge and sharpen my presentation of the topic. As the manuscript of the first edition of that volume approached its final form, Ven. Phra Ñāṇavarodom, Bhikkhu Bodhi, Thiradhammo Bhikkhu, Amaro Bhikkhu, Suviro Bhikkhu, Bill Weir, and Doris Weir all read copies of it and offered valuable suggestions for improvement.</p>

    <p>In the original conception of this book I planned only one volume, explaining the Pāṭimokkha rules. However, in 1997, Phra Ajaan Suwat Suvaco (Phra Bodhidhammācariya Thera) convinced me that my work would not be complete until I had added the second volume, on the Khandhaka rules, as well. In the course of researching that volume, I had the opportunity to deepen my knowledge not only of the Khandhakas but also of areas in the Sutta Vibhaṅga that I had previously overlooked or misapprehended. Thus was born the idea for the current revision. My aim in carrying it out has been twofold, both to correct errors and deficiencies in the first edition and to shape the two volumes into a more coherent whole. This second aim has involved reorganizing the material and adopting a more consistent and accurate translation scheme for technical terms. The revision was given added impetus from the questions I received from my students during Vinaya classes here at the monastery, and from a series of critiques and questions I received from bhikkhus in other locations. In addition to critiques from an anonymous group of bhikkhus in Sri Lanka, I also received critiques from Ven. Jotipālo Bhikkhu, Brahmavaṁso Bhikkhu, Brahmāli Bhikkhu, and the late Paññāvuḍḍho Bhikkhu on volume one, and an extended critique from Ven. Bhikkhu Ñāṇatusita on volume two. All of these critiques, even in the many areas in which I disagreed with them, have helped sharpen the focus of the book and made the presentation more accurate and complete. I am grateful for the time that my fellow bhikkhus have devoted to making this work more useful and reliable. Many lay people have provided help as well, in particular Thomas Patton, who provided references to the Burmese edition of the Canon, and Olivia Vaz and V.A. Ospovat, who helped with the proofreading. I, of course, remain responsible for any errors it may still contain.</p>

    <p>For anyone familiar with the first edition of this book, the most obvious change will be the book’s increased size. This is the result of a felt need to make its coverage more comprehensive. In the first instance, this has meant providing a more detailed account of the material in the Canon and commentaries. This in turn has uncovered more points where the commentaries conflict with the Canon, all of which required determining what seemed to be the most correct interpretation of the points in question. I have also found it necessary to take into account the variant readings found in the four major editions of the Canon: Thai, Sri Lankan, Burmese, and European PTS. In the first edition of this book I limited my attention to the Thai edition, but I have since come to realize the need to sift through all four editions to find the best readings for the rules and their explanatory material. This point I discuss in detail in the Introduction to volume one. What it means in practice is that when the variant readings touch on important issues and would clearly make a practical difference, I have had to devote a fair amount of space to explaining my preference for one over the others. At first I wanted to avoid dealing with these issues in the body of the book, but given the still unsettled nature of our current knowledge of the Canon, I found them unavoidable. I hope that these discussions will not interfere with understanding the general thrust of each rule. Again, if you are new to the subject of Buddhist monastic discipline, you can skip over these scholarly discussions during your first read-through. Then, when your knowledge of the Vinaya is more solid and you feel so inclined, you can return to them at a later time.</p>

    <p>Although my general policy has been to accept the most coherent reading regardless of which edition it appears in, I have had to depart from this policy in one area, that of the transaction statements used in Community meetings. Each edition has its own standards for determining word order and orthography for these statements, and in almost all cases these variant standards make no practical difference. Thus, instead of trying to establish a preferred reading in every case, I have—for consistency’s sake—followed the Thai standard throughout, and have noted variants only where they seem important.</p>

    <p>One last practical note: Even though I have consulted all four major editions of the Canon, I have provided reference numbers only to one—the PTS edition—as that is the edition most readily available to my readers. References to the commentaries have been handled as follows: When, in the course of discussing rule <em>x,</em> I cite the Commentary to rule <em>x,</em> I simply say, “The Commentary says ....” When I augment the discussion of rule <em>x</em> with a citation from the Commentary to rule <em>y</em>, I say, “The Commentary to rule <em>y</em> says ....” These references may then be easily found in the area of the Commentary devoted to the relevant rule, <em>x</em> or <em>y</em>, regardless of the edition consulted.</p>

    <p>When the first editions of volumes one and two were printed, the primary dedicatees were still alive. Both, however, have since passed away, but my respect and gratitude to them have not diminished. So I now dedicate the volumes to their memory. In the case of this first volume, that dedication is to the memory of my preceptor, Phra Debmoli (Samrong Guṇavuḍḍho) of Wat Asokaram, Samut Prakaan, Thailand, as well as to all my other teachers in the path of the Dhamma-Vinaya.</p>

    <p class="signatures">Ṭhānissaro Bhikkhu</p>

    <p class="signatures">(Geoffrey DeGraff)</p>

    <div class="address">
      <p>Metta Forest Monastery</p>

      <p>Valley Center, CA 92082-1409 U.S.A.</p>

      <p>May, 2007</p>
    </div>

    <p>This third revised edition was inspired by questions from many of my fellow bhikkhus, in particular Vens. Nyanadhammo, Jotipālo, Khematto, and Kusalī.</p>

    <div class="address">
      <p>November, 2013</p>
    </div>
  </div><!--end preface-->
</body>
</html>
